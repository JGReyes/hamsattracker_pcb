HAMSatTracker (PCB)
================

En este repositorio se encuentran los archivos de diseño de la PCB en KiCad utilizada en el proyecto de la antena motorizada,
así como una lista de componentes y archivo para cálculos térmicos en LTSpice.

![PCB_3D](https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEik5tcJfIN48aYMIMySsajtkcVQVO1KSK_x4mDb1JUOqmM3UBzRKSTQLXbfetHX70-1TOH4_4CXbu9ZyRmbNI_w7NELtMduKQms6S6zoZqwScMe-PjfQCkx799_wEadcwZaFautRP_oKSlUsUvIHqr-T63XntbVyQZauxlr4yQzQraUHmdPXSehgnNpIw/s1919/placa2.png)

Las principales características de la placa son las siguientes:

- Entrada de 9V-36V DC por conector XT60 o terminal atornillado.
- Protección por polaridad inversa.
- Protección con diodo TVS para la salida de 5V.
- Fusible rearmable de 4A para la entrada del conversor DC/DC.
- Fusible rearmable de 10A para alimentación de los drivers de los motores.
- Conversor ADC para voltaje de batería con dos entradas adicionales.
- Conectores opcionales para poder conectar el puerto serie, bus I2C, bus SPI, 2 entradas sobrantes al ADC,
  pines no utilizados en la Raspberry Pi y salida de 5V para otros dispositivos.


Para más información consultar el artículo sobre la PCB en el [blog.](https://www.circuiteando.net/2023/04/hamsattracker-parte-2-electronica.html)
